package ru.t1.stepanishchev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.dto.request.ProjectCreateRequest;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-create";

    @NotNull
    private final String DESCRIPTION = "Create new project.";

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = TerminalUtil.nextLine();
        String token = getToken();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken(), name, description);
        getProjectEndpoint().createProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}