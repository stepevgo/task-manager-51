package ru.t1.stepanishchev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.model.UserDTO;
import ru.t1.stepanishchev.tm.dto.request.UserProfileRequest;
import ru.t1.stepanishchev.tm.dto.response.UserProfileResponse;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.exception.entity.UserNotFoundException;
import ru.t1.stepanishchev.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "view-user-profile";

    @NotNull
    private final String DESCRIPTION = "view profile of current user.";

    @Override
    public void execute() {
        @NotNull UserProfileResponse response = getUserEndpoint().viewProfileUser(new UserProfileRequest(getToken()));
        @Nullable final UserDTO user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}