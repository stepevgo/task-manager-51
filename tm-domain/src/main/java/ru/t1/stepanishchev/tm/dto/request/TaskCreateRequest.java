package ru.t1.stepanishchev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskCreateRequest(
            @Nullable final String token,
            @Nullable final String name,
            @Nullable final String description) {
        super(token);
        this.name = name;
        this.description = description;
    }

}