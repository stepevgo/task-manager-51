package ru.t1.stepanishchev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.model.ITaskRepository;
import ru.t1.stepanishchev.tm.api.repository.model.IUserRepository;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.model.ITaskService;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.enumerated.TaskSort;
import ru.t1.stepanishchev.tm.exception.entity.StatusEmptyException;
import ru.t1.stepanishchev.tm.exception.entity.TaskNotFoundException;
import ru.t1.stepanishchev.tm.exception.entity.UserNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.*;
import ru.t1.stepanishchev.tm.model.Task;
import ru.t1.stepanishchev.tm.model.User;
import ru.t1.stepanishchev.tm.repository.model.TaskRepository;
import ru.t1.stepanishchev.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Collection;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository>
        implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public ITaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @Override
    @NotNull
    public String getSortType(@Nullable final TaskSort sort) {
        if (sort == TaskSort.BY_CREATED) return "created";
        if (sort == TaskSort.BY_STATUS) return "status";
        else return "name";
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @NotNull Task task = new Task();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();

        task.setUser(user);
        task.setName(name);
        task.setDescription(description);
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @Nullable String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @Nullable Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();

        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    @NotNull
    @SneakyThrows
    public Task removeById(
            @Nullable String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();

        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @Nullable Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();

        task.setStatus(status);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear(userId);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable String userId, @Nullable TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        try {
            return repository.findAll(userId, getSortType(sort));
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        try {
            @Nullable final Task task = repository.findOneById(userId, id);
            return task;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void set(@NotNull final Collection<Task> tasks) {
        if (tasks.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.addAll(tasks);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        try {
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

}